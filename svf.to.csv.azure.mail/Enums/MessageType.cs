﻿namespace svf.to.csv.azure.mail.Enums {
    public enum MessageType {
        SvfToCsvConversion_Success,
        SvfToCsvConversion_Failure
    }
}