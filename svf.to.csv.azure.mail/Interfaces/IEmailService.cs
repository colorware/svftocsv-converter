﻿using svf.to.csv.azure.mail.Enums;

namespace svf.to.csv.azure.mail.Interfaces {
    public interface IEmailService {
        void Send(MessageType messageType, string recipients, string svfFilename, string csvFilename, string message);
    }
}