﻿using System;
using System.Net.Mail;

using svf.to.csv.azure.mail.Enums;
using svf.to.csv.azure.mail.Interfaces;
using svf.to.csv.azure.mail.Messages;
using svf.to.csv.utilities;

namespace svf.to.csv.azure.mail.MailJet {
    public class MailJetWrapper : IEmailService {
        public void Send(MessageType messageType, string recipients, string svfFilename, string csvFilename,
                         string message) {
            if (!ConfigValueReader.IsProductionMode) {
                return;
            }

            var msgData =
                MessageFactory.CreateMessage(messageType, svfFilename, csvFilename, message);

            try {
                sendCustomerMessage(recipients, msgData);
            }
            catch {
                // TODO
            }
        }

        //---------------------------------------------------------------------

        private void sendCustomerMessage(string recipients, IMessage msgData) {
            if (string.IsNullOrWhiteSpace(recipients)) return;

            MailMessage customerMsg = new MailMessage {
                // this address must be verified on mailjet!
                // currently only my colorware email address is verified
                From = new MailAddress("jaco@colorware.eu", "Colorware"),
                Subject = msgData.Subject,
                Body = msgData.Body
            };
            msgData.Attachments.ForEach(x => customerMsg.Attachments.Add(x));

            sendMessage(recipients, customerMsg);
        }

        //---------------------------------------------------------------------

        private void sendMessage(string recipients, MailMessage customerMsg) {
            var recipientList = recipients.Split(new[] {' ', ',', ';'}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var recipient in recipientList) {
                if (!isValidEmail(recipient)) {
                    continue;
                }

                var addressTokens = recipient.Split('@');
                if (addressTokens.Length > 0) {
                    var name = addressTokens[0];
                    customerMsg.To.Add(new MailAddress(recipient, name));
                }
            }

            sendMessage(customerMsg);
        }

        //---------------------------------------------------------------------

        private void sendMessage(MailMessage msg) {
            if (msg.To.Count == 0) return;

            SmtpClient client = new SmtpClient(ConfigValueReader.MailjetHost, ConfigValueReader.MailjetPort) {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials =
                    new System.Net.NetworkCredential(ConfigValueReader.MailjetApiKey,
                                                     ConfigValueReader.MailjetSecretKey)
            };

            client.Send(msg);
        }

        //---------------------------------------------------------------------

        private bool isValidEmail(string emailaddress) {
            try {
                new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException) {
                return false;
            }
        }

        //---------------------------------------------------------------------
    }
}