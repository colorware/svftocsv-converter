﻿using System.Collections.Generic;
using System.Net.Mail;

namespace svf.to.csv.azure.mail.Messages {
    public class BaseMessage : IMessage {
        private readonly string svfFilename;
        private readonly string csvFilename;

        public string Subject { get; }
        public string Body { get; }

        public List<Attachment> Attachments {
            get {
                List<Attachment> attachments = new List<Attachment>();
                if (!string.IsNullOrWhiteSpace(svfFilename)) attachments.Add(new Attachment(svfFilename));
                if (!string.IsNullOrWhiteSpace(csvFilename)) attachments.Add(new Attachment(csvFilename));

                return attachments;
            }
        }

        protected BaseMessage(string subject, string body, string svfFilename, string csvFilename = "") {
            Subject = subject;
            Body = body;
            this.svfFilename = svfFilename;
            this.csvFilename = csvFilename;
        }
    }
}