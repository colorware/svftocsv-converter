﻿namespace svf.to.csv.azure.mail.Messages {
    internal class ConvertNotificationFailure : BaseMessage {
        public ConvertNotificationFailure(string svfFilename, string message)
            : base(subject: "SVF to CSV conversion FAILED!",
                   body: $"The SVF file {svfFilename} could not be converted to create a CSV file.  Reason:  {message}",
                   svfFilename: svfFilename) { }
    }
}