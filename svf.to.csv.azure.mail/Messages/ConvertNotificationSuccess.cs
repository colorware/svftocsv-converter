﻿namespace svf.to.csv.azure.mail.Messages {
    internal class ConvertNotificationSuccess : BaseMessage {
        public ConvertNotificationSuccess(string svfFilename, string csvFilename)
            : base(subject: "SVF to CSV conversion SUCCEEDED!",
                   body: $"The SVF file {svfFilename} was successfully converted to create the CSV file {csvFilename}",
                   svfFilename: svfFilename,
                   csvFilename: csvFilename) { }
    }
}