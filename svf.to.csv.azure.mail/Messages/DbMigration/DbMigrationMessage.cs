﻿using System;

namespace mc.azure.email.Messages.DbMigration {
    public class DbMigrationMessage : BaseMessage {
        public override bool SendClientMessage => false;

        public override string Subject => throw new NotImplementedException();
        public override string Body => throw new NotImplementedException();

        public override string SupportSubject => "Database migrations report";

        public override string SupportBody => "The following database migrations were performed:"
                                              + $"{Environment.NewLine}"
                                              + "================================================="
                                              + $"{Environment.NewLine}"
                                              + $"{Environment.NewLine}"
                                              + $"{supportMessage}";

        public DbMigrationMessage(string customerName, string customerLocation,
                                  string customerUrl, string supportMessage)
            : base(customerName, customerLocation, customerUrl, supportMessage) { }
    }
}