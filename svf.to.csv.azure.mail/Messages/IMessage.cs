﻿using System.Collections.Generic;
using System.Net.Mail;

namespace svf.to.csv.azure.mail.Messages {
    public interface IMessage {
        string Subject { get; }
        string Body { get; }
        List<Attachment> Attachments { get; }
    }
}