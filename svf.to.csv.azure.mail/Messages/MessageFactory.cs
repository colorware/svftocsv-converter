﻿using System.ComponentModel;

using svf.to.csv.azure.mail.Enums;

namespace svf.to.csv.azure.mail.Messages {
    public static class MessageFactory {
        public static IMessage CreateMessage(MessageType messageType, string svfFilename, string csvFilename,
                                             string message) {
            switch (messageType) {
                case MessageType.SvfToCsvConversion_Success:
                    return new ConvertNotificationSuccess(svfFilename, csvFilename);

                case MessageType.SvfToCsvConversion_Failure:
                    return new ConvertNotificationFailure(svfFilename, message);

                default:
                    throw new InvalidEnumArgumentException(nameof(messageType), (int)messageType, typeof(MessageType));
            }
        }
    }
}