﻿Imports System.IO
Imports System.Text

Public Class CsvFileExporter
    Public Messages As StringBuilder = New StringBuilder()

    ' TODO - should height not be an Integer type?
    Public Function Export(data As DataTable, colorBarName As String, colorBarDescr As String,
                      patchWidth As Double, patchHeight As Double,
                      patchesPerZone As Integer, usedKeys As Integer, csvDirectory As String) As String
        Dim filename = ""
        Try
            Dim sb = new StringBuilder()

            'If File.Exists(fileName) Then
                sb.AppendLine("color_strip.name,color_strip.description,color_strip.rowcount,color_strip.repeats,color_strip.patchwidth,color_strip.patchheight,color_strip.patches_per_zone,color_strip.number_of_zones,percentage,ink_zone,name,patch_type,tolerance_group,slot1,slot2,slot3,sort_order")

                For i = 0 To data.Rows.Count - 1
                    '*** last chance check for unassigned patches ***
                    If data.Rows(i).Item(3).ToString() = "" Then
                        data.Rows(i).Item(3) = "SKIP"
                        data.Rows(i).Item(7) = "-1"
                        data.Rows(i).Item(8) = "-1"
                        data.Rows(i).Item(9) = "-1"
                    End If

                    sb.Append(colorbarName & ",")
                    sb.Append(colorBarDescr & ",")
                    sb.Append("1" & ",")
                    sb.Append("false" & ",")
                    sb.Append(patchWidth & ",")
                    sb.Append(patchHeight & ",")
                    sb.Append(PatchesPerZone & ",")
                    sb.Append(usedKeys & ",")
                    sb.Append(CStr(data.Rows(i).Item(1)) & ",")
                    sb.Append(CStr(data.Rows(i).Item(2)) & ",")
                    sb.Append(data.Rows(i).Item(3) & ",")
                    sb.Append(CStr(data.Rows(i).Item(4)) & ",")
                    sb.Append(CStr(data.Rows(i).Item(6)) & ",")
                    sb.Append(CStr(data.Rows(i).Item(7)) & ",")
                    sb.Append(CStr(data.Rows(i).Item(8)) & ",")
                    sb.Append(CStr(data.Rows(i).Item(9)) & ",")
                    sb.AppendLine(i & ",")
                Next
            'End If

            filename = Path.Combine(csvDirectory, colorBarName + ".csv")
            File.WriteAllText(filename, sb.ToString())

        Catch ex As Exception
            Messages.AppendLine("Error saving " & colorbarName)
            Messages.AppendLine($"Exception:  {ex.Message}")
        End Try

        Return filename
    End Function
End Class