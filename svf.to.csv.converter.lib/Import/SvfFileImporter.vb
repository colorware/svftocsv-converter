﻿Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions

Public Class SvfFileImporter
    Public SvfFile As SvfFile = New SvfFile()   ' Declare SVF as type LoadedSVF

    Public Data As DataTable

    Public ReadOnly InkNameArray(12) As String                ' InkNames and IndexNumbers from the measurement
    Public ReadOnly InkIndexArray(12) As Integer

    Private col1Name As String = "patch#"
    Private col2Name As String = "perc."
    Private col3Name As String = "inkzone"
    Private col4Name As String = "name"
    Private col5Name As String = "patch type"
    Private col6Name As String = "tower"
    Private col7Name As String = "tolerance group"
    Private col8Name As String = "slot1"
    Private col9Name As String = "slot2"
    Private col10Name As String = "slot3"
    Private col11Name As String = "xPos"

    Public Messages as StringBuilder = New StringBuilder()

    public Sub ImportSvf(filename As String)
        Try
            ConstructDataTable()

            Using sr As New StreamReader(filename)  ' Open the file using a stream reader.
                Dim Original As String
                Original = sr.ReadToEnd()           ' read all from filename
                SvfFile = RetrieveSVFData(Original)     ' extract SVFproperties
            End Using

            CleanColorIndex()                   ' trim and replace color names
            CalculatePatchConfig()              ' calculate keys and widths 
            DecodeInkNamesFromTowers()          ' lookup the ink name from the InkName and Ink_IDX table
            ConvertPatchesInGrid()              ' convert the values in the grid to something like a MeasureColor Color Bar definition
            
        Catch ex As Exception
            Messages.AppendLine($"An exception occurred :  {ex.Message}")
        End Try


        'Dim builder = New StringBuilder()
        'For i = 0 To Data.Rows.Count - 1
        '    For j = 0 To Data.Columns.Count - 1
        '        builder.Append($"{Data.Rows(i).Item(j)}")
        '        builder.Append(" ")
        '    Next
        '    builder.AppendLine()
        'Next

        'Dim output = builder.ToString()


    End Sub

    Private Function RetrieveSVFData(filedata As String) As SvfFile
        Try
            Dim values As String()                              'Holds the values splitted from the lines
            Dim isPatch = 2000                       'Last line before PatchData
            Dim lines = Filedata.Split(vbLf)                        'Split Up FileData in dicrete lines
            Dim lastLine As Integer = lines.GetUpperBound(0)

            For i = 0 To lastLine                               'Loop through all lines to gather the desired info
                'decide whether or not this is patchdata
                values = lines(i).Split(vbTab)                  'split Up line in separate values
                If i < isPatch Then
                    Select Case CStr(values(0))                 'select action based upon first value
                        Case "PressName:"
                            SvfFile.PressName = values(1)
                        Case "NumberKeys:"
                            SvfFile.Keys = CInt(values(1))
                        Case "KeySize (mm):"
                            SvfFile.Keysize = CDbl(values(1))
                        Case "NumberPrintingUnits:"
                            SvfFile.InkSlots = CInt(values(1))
                        Case "ColorbarName:"
                            SvfFile.OriginalColorBarName = values(1)
                        Case "NumberOfInks:"
                            SvfFile.InkNumber = CInt(values(1))
                        Case "PrintingUnit:"
                            For j = 1 To values.GetUpperBound(0)
                                InkIndexArray(j) = CInt(values(j))
                            Next
                        Case "InkNames:"
                            For j = 1 To values.GetUpperBound(0)
                                InkNameArray(j) = values(j)
                            Next
                        Case "Patch#"
                            isPatch = i + 1
                    End Select
                Else
                    If values(0) <> "Patch#" And values.GetUpperBound(0) > 20 Then               'must absolutely be patch data
                        AddDataRow(values(0), "", values(1), "", values(3), values(4), "", "", "", "", values(2))
                    End If
                End If
            Next
            RetrieveSVFData = SvfFile
        Catch
            Messages.AppendLine("Error: Retrieving SVF Data failed.")
            RetrieveSVFData = Nothing
        End Try
    End Function

    Private Sub CleanColorIndex()
        Try
            ' first strip color indexing values, e.g. remove "(1.2)" from "m(1.2)" etc.
            ' this algorithm assumes that a colour name is not just e.g. "(12.34)"
            For i = 0 To InkNameArray.Length - 1
                If String.IsNullOrWhiteSpace(InkNameArray(i)) Then Continue For

                Dim newInkName = Regex.Replace(InkNameArray(i).Trim(), "\(\d{1,2}.\d{1,2}\)$", "")
                InkNameArray(i) = newInkName.Trim()
            Next

            For i = 0 To InkNameArray.Length - 1     
                If String.IsNullOrWhiteSpace(InkNameArray(i)) Then Continue For
                
                ' loop through inkslots to adopt popular color names
                If Not (IsNothing(InkNameArray(i))) Then
                    If InkNameArray(i).ToLower.Contains("cyan") Then InkNameArray(i) = "C"
                    If InkNameArray(i).ToLower.Contains("magenta") Then InkNameArray(i) = "M"
                    If InkNameArray(i).ToLower.Contains("yellow") Then InkNameArray(i) = "Y"
                    If InkNameArray(i).ToLower.Contains("black") Then InkNameArray(i) = "K"
                End If
            Next

            Dim spot = 0
            For i = 0 To InkNameArray.Length - 1
                If String.IsNullOrWhiteSpace(InkNameArray(i)) Then Continue For

                If IsNothing(InkNameArray(i)) Then
                    spot = spot + 1
                    InkNameArray(i) = "S" + CStr(spot)
                Else
                    Select Case InkNameArray(i)
                        Case "C", "M", "Y", "K"
                            Continue For
                        Case Else
                            spot = spot + 1
                            InkNameArray(i) = "S" + CStr(spot)
                    End Select
                End If
            Next

            'Colors = "Found " & CStr(SVF.InkSlots) & ": "
            'For i = 1 To SVF.InkSlots
            'Colors = Colors + "Slot " + CStr(Ink_IDX(i)) + ": " + InkName(i) + ", "
            'Next
            'Messages.AppendLine(Colors)
        Catch
            Messages.AppendLine("Could Not clean the Color names")
        End Try
    End Sub

    Private Sub CalculatePatchConfig()
        Dim endKey As Integer                   ' last key in measurement
        Dim midKey As Integer                   ' mid key in measurement 
        Dim getKey As Integer                   ' trouble free key checking
        Dim patCnt As Integer                   ' patchcount for center key       
        endKey = 0

        '*** get startkey from first measured patch ****
        SvfFile.StartKey = CInt(data.Rows(0).Item(2))
        'Messages.AppendLine(SVF.StartKey)

        '*** scan across the datagrid to find all keys ***
        For i = 0 To data.Rows.Count - 1
            If data.Rows(i).Item(2) IsNot Nothing Then
                getKey = CInt(data.Rows(i).Item(2))
                If getKey > endKey Then endKey = getKey
            End If
        Next

        SvfFile.UsedKeys = (endKey - SvfFile.StartKey) + 1    ' How many keys used

        If SvfFile.UsedKeys = 1 Then
            SvfFile.UsedKeys = 1      ' One key bar
            midKey = 1
            SvfFile.PatchesPerZone = 0
            patCnt = data.Rows.Count
            SvfFile.PatchWidth = (CInt(data.Rows(data.Rows.Count - 1).Item(10)) - CInt(data.Rows(0).Item(10))) / (data.Rows.Count - 1)
            'Messages.AppendLine("laatste Patch is " & CStr(data.Rows(data.Rows.Count - 2).Item(10)))
        Else
            midKey = SvfFile.UsedKeys / 2 + SvfFile.StartKey    ' what's the center key
            '*** scan across the datagrid to count the patches in the center key ***
            For i = 1 To data.Rows.Count - 1
                If data.Rows(i).Item(2) IsNot Nothing Then
                    getKey = data.Rows(i).Item(2)
                    If getKey = midKey Then
                        patCnt = patCnt + 1
                    End If
                End If
            Next
            SvfFile.PatchesPerZone = patCnt
            SvfFile.PatchWidth = SvfFile.Keysize / patCnt
        End If

        '*** save patchwidth when X is not given but we know the zonewidth and the patches per zone
        If Math.Abs(SvfFile.PatchWidth) < 0.001 Then
            Messages.AppendLine("patchWidth=0")
            If SvfFile.Keysize > 0 And SvfFile.PatchesPerZone > 1 Then
                Messages.AppendLine("But we do have the number of patches per zone and the zonewidth")
                SvfFile.PatchWidth = SvfFile.Keysize / SvfFile.PatchesPerZone
            End If
        End If
        'Messages.AppendLine("PatchWidth " & CStr(SVF.PatchWidth))

    End Sub

    Private Sub DecodeInkNamesFromTowers()

        Dim getTowers As String                    ' original patch decription read-out
        Dim colorNumbers(3) As String              ' split from the towers text
        Dim patchName As String                    ' combined towers to colors

        Try
            '*** scan across the datagrid to process all patches ***
            For i = 0 To data.Rows.Count - 1
                getTowers = data.Rows(i).Item(5)
                If Not (getTowers Is Nothing) Then
                    patchName = ""
                    Select Case getTowers.Length
                        Case 1
                            colorNumbers(0) = getTowers
                            'PatchName = InkName(Ink_IDX(Val(ColorNumbers(0))))
                            'patchName = patchName + InkName(Ink_IDX(CInt(colorNumbers(j - 1))))
                            Dim indexOfTower = Array.FindIndex(InkIndexArray, Function(s) s = CInt(colorNumbers(0)))
                            If indexOfTower = -1 Then indexOfTower = 0
                            patchName = InkNameArray(indexOfTower)
                            data.Rows(i).Item(7) = Val(colorNumbers(0))
                            data.Rows(i).Item(8) = -1
                            data.Rows(i).Item(9) = -1
                        Case 3
                            colorNumbers = getTowers.Split("+")
                            For j = 0 To 1
                                'patchName = patchName + InkName(Ink_IDX(CInt(colorNumbers(j - 1))))
                                Dim indexOfTower = Array.FindIndex(InkIndexArray, Function(s) s = CInt(colorNumbers(j)))
                                patchName = patchName + InkNameArray(indexOfTower)
                            Next
                            data.Rows(i).Item(7) = Val(colorNumbers(0))
                            data.Rows(i).Item(8) = Val(colorNumbers(1))
                            data.Rows(i).Item(9) = -1
                        Case 5
                            colorNumbers = getTowers.Split("+")
                            For j = 0 To 2
                                'patchName = patchName + InkName(Ink_IDX(CInt(colorNumbers(j - 1))))
                                Dim indexOfTower = Array.FindIndex(InkIndexArray, Function(s) s = CInt(colorNumbers(j)))
                                patchName = patchName + InkNameArray(indexOfTower)
                            Next
                            data.Rows(i).Item(7) = Val(colorNumbers(0))
                            data.Rows(i).Item(8) = Val(colorNumbers(1))
                            data.Rows(i).Item(9) = Val(colorNumbers(2))
                    End Select
                    data.Rows(i).Item(3) = patchName
                Else
                    data.Rows(i).Item(3) = "SKIP"
                    data.Rows(i).Item(7) = -1
                    data.Rows(i).Item(8) = -1
                    data.Rows(i).Item(9) = -1
                End If

                patchName = data.Rows(i).Item(4)

                If Not (patchName Is Nothing) Then
                    If patchName.Length > 2 Then
                        If patchName.Substring(0, 2) = "MW" Or patchName.Substring(0, 2) = "ID" Then
                            data.Rows(i).Item(3) = patchName
                        End If
                    End If
                End If
            Next
        Catch
            Messages.AppendLine("Cannot Resolve color names to individual patches")
        End Try

        ' build color editor map for KCMY - but what about other colors? what if a non-production color is to initial slots? then it must be mapped to 4, 5, ... (ie the spot color range)
        MapTowersToColorBarEditor()         ' convert tower numbers to color bar editor indices

    End Sub

    Private Sub MapTowersToColorBarEditor()
        Dim map = new TowerToColorBarEditorMap(InkNameArray, InkIndexArray)

        For i = 0 To Data.Rows.Count - 1
            Try
                Data.Rows(i).Item(7) = map.GetColorBarEditorIndexForTowerIndex(Data.Rows(i).Item(7))
                Data.Rows(i).Item(8) = map.GetColorBarEditorIndexForTowerIndex(Data.Rows(i).Item(8))
                Data.Rows(i).Item(9) = map.GetColorBarEditorIndexForTowerIndex(Data.Rows(i).Item(9))
            Catch ex As Exception
                Messages.AppendLine($"An exception occurred while mapping tower indices to ColorBar Editor indices :  {ex.Message}")
            End Try
        Next
    End Sub

    Private Sub ConvertPatchesInGrid()

        Dim getKey As String                    ' original patch decription read-out

        '*** scan across the datagrid to process all patches ***
        For i = 0 To data.Rows.Count - 1
            If data.Rows(i).Item(4) IsNot Nothing Then
                getKey = data.Rows(i).Item(4)
                If getKey IsNot Nothing Then
                    If getKey.Length > 3 Then
                        getKey = getKey.Substring(0, 3)
                        Select Case getKey
                            Case "Sol"
                                FixSolidPatch(i)
                            Case "Ove"
                                FixOverPrintpatch(i)
                            Case "Pap"
                                FixPaperPatch(i)
                            Case "Tin"
                                FixDotGainPatch(i)
                            Case "Gra"
                                FixGrayBalancePatch(i)
                            Case "MW3"
                                FixSpecial(i)
                            Case "ID_"
                                FixSpecial(i)
                            Case Else
                                FixUndefinedPatch(i)
                        End Select
                    End If
                End If
            End If
        Next
        'data.Update()
    End Sub

    Private Sub FixSolidPatch(row As Integer)
        Dim patchName As String = data.Rows(row).Item(3).ToString()

        data.Rows(row).Item(1) = "100"    ' percentage
        data.Rows(row).Item(4) = "1"      ' patch type

        If Not String.IsNullOrEmpty(patchName) Then
            Select Case patchName.Substring(0, 1)
                Case "S"
                    data.Rows(row).Item(6) = "3"    ' tolerance group
                Case Else
                    data.Rows(row).Item(6) = "1"    ' tolerance group
            End Select
        Else                                                        ' this inkslot was not defined in the SVF, patch is undefined
            data.Rows(row).Item(1) = "100"    ' percentage
            data.Rows(row).Item(3) = "SKIP"   ' name
            data.Rows(row).Item(4) = "0"      ' patch type
            data.Rows(row).Item(6) = "0"      ' tolerance group
            data.Rows(row).Item(7) = "-1"     ' slot1
            data.Rows(row).Item(8) = "-1"     ' slot2
            data.Rows(row).Item(9) = "-1"     ' slot3
        End If
    End Sub

    Private Sub FixOverPrintpatch(row As Integer)
        data.Rows(row).Item(1) = "100"  ' percentage
        data.Rows(row).Item(4) = "9"    ' patch type
        data.Rows(row).Item(6) = "2"    ' tolerance group
    End Sub

    Private Sub FixPaperPatch(row As Integer)
        data.Rows(row).Item(1) = "0"    ' percentage
        data.Rows(row).Item(3) = "PW"   ' name
        data.Rows(row).Item(4) = "5"    ' patch type
        data.Rows(row).Item(6) = "6"    ' tolerance group
        data.Rows(row).Item(7) = "-1"   ' slot1
        data.Rows(row).Item(8) = "-1"   ' slot2
        data.Rows(row).Item(9) = "-1"   ' slot3
    End Sub

    Private Sub FixDotGainPatch(row As Integer)
        Dim patchDescription As String
        Dim percentage As Integer

        patchDescription = data.Rows(row).Item(4).ToString()

        If patchDescription <> "" Then
            percentage = Val(patchDescription.Substring(4))
            data.Rows(row).Item(1) = percentage   ' percentage
            data.Rows(row).Item(4) = "2"    ' patch type
            data.Rows(row).Item(6) = "7"    ' tolerance group
        Else
            data.Rows(row).Item(1) = "100"    ' percentage
            data.Rows(row).Item(3) = "SKIP"   ' name
            data.Rows(row).Item(4) = "0"    ' patch type
            data.Rows(row).Item(6) = "0"    ' tolerance group
            data.Rows(row).Item(7) = "-1"   ' slot1
            data.Rows(row).Item(8) = "-1"   ' slot2
            data.Rows(row).Item(9) = "-1"   ' slot3
        End If
    End Sub

    Private Sub FixGrayBalancePatch(row As Integer)
        Dim patchName As String = data.Rows(row).Item(4)
        Dim newPatchName = ""

        data.Rows(row).Item(1) = "100"    ' percentage
        data.Rows(row).Item(7) = "-1"   ' slot1
        data.Rows(row).Item(8) = "-1"   ' slot2
        data.Rows(row).Item(9) = "-1"   ' slot3

        Select Case patchName
            Case "Gray25-19-19"
                newPatchName = "BHG7"
            Case "Gray27-19-20"
                newPatchName = "BHGC"
            Case "Gray20-12-12"
                newPatchName = "BHMC"
            Case "Gray20-14-13"
                newPatchName = "BHPV"
            Case "Gray40-41-41"
                newPatchName = "BMBR"
            Case "Gray50-40-40"
                newPatchName = "BMG7"
            Case "Gray45-36-36"
                newPatchName = "BMGC"
            Case "Gray40-27-27"
                newPatchName = "BMMC"
            Case "Gray40-29-28"
                newPatchName = "BMPV"
            Case "Gray75-66-66"
                newPatchName = "BSG7"
            Case "Gray66-56-56"
                newPatchName = "BSGC"
            Case "Gray80-65-65"
                newPatchName = "BSMC"
            Case "Gray80-70-68"
                newPatchName = "BSPV"
            Case "Gray74-60-60"
                newPatchName = "BSTK"
            Case "Gray75-62-60"
                newPatchName = "BSUG"
            Case Else
                newPatchName = "SKIP"
        End Select

        data.Rows(row).Item(3) = newPatchName   ' patch name

        Select Case newPatchName.Substring(0, 2)
            Case "BS"
                data.Rows(row).Item(4) = "8"    ' patch type
                data.Rows(row).Item(6) = "5"    ' tolerance group
            Case "BM"
                data.Rows(row).Item(4) = "7"    ' patch type
                data.Rows(row).Item(6) = "5"    ' tolerance group
            Case "BH"
                data.Rows(row).Item(4) = "6"    ' patch type
                data.Rows(row).Item(6) = "5"    ' tolerance group
            Case "SK"
                data.Rows(row).Item(4) = "0"    ' patch type
                data.Rows(row).Item(6) = "0"    ' tolerance group
        End Select
    End Sub

    Private Sub FixSpecial(row)
        data.Rows(row).Item(1) = "100"    ' percentage
        data.Rows(row).Item(4) = "10"    ' patch type
        data.Rows(row).Item(6) = "8"    ' tolerance group
        data.Rows(row).Item(7) = "-1"   ' slot1
        data.Rows(row).Item(8) = "-1"   ' slot2
        data.Rows(row).Item(9) = "-1"   ' slot3
    End Sub

    Private Sub FixUndefinedPatch(row)
        data.Rows(row).Item(1) = "100"    ' percentage
        data.Rows(row).Item(3) = "SKIP"   ' name
        data.Rows(row).Item(4) = "10"    ' patch type
        data.Rows(row).Item(6) = "8"    ' tolerance group
        data.Rows(row).Item(7) = "-1"   ' slot1
        data.Rows(row).Item(8) = "-1"   ' slot2
        data.Rows(row).Item(9) = "-1"   ' slot3
    End Sub

    Private sub ConstructDataTable()
        data  = new DataTable()

        ' add 11 columns
        data.Columns.Add(New DataColumn(col1Name))
        data.Columns.Add(New DataColumn(col2Name))
        data.Columns.Add(New DataColumn(col3Name))
        data.Columns.Add(New DataColumn(col4Name))
        data.Columns.Add(New DataColumn(col5Name))
        data.Columns.Add(New DataColumn(col6Name))
        data.Columns.Add(New DataColumn(col7Name))
        data.Columns.Add(New DataColumn(col8Name))
        data.Columns.Add(New DataColumn(col9Name))
        data.Columns.Add(New DataColumn(col10Name))
        data.Columns.Add(New DataColumn(col11Name))
    End sub

    Private Sub AddDataRow(col1 As String, col2 As String, col3 As String, col4 As String, col5 As String, col6 As String, col7 As String, 
                           col8 As String, col9 As String, col10 As String, col11 As String)
        Dim row As DataRow = data.NewRow()

        row(col1Name) = col1
        row(col2Name) = col2
        row(col3Name) = col3
        row(col4Name) = col4
        row(col5Name) = col5
        row(col6Name) = col6
        row(col7Name) = col7
        row(col8Name) = col8
        row(col9Name) = col9
        row(col10Name) = col10
        row(col11Name) = col11

        data.Rows.Add(row)
    End Sub

End Class