﻿Public Class TowerToColorBarEditorMap
    Public Class MapInfo
        Public InkName As String
        Public InkTower As Integer
        Public ColorBarEditorIndex As Integer

        Public Sub New (inkName As String, inkTower As Integer)
            InkName = inkName
            InkTower = inkTower
            ColorBarEditorIndex = -1
        End Sub

        Public Overrides Function ToString() As String
            Return $"name:  {InkName}  -  tower:  {InkTower}  -  CB index:  {ColorBarEditorIndex}"
        End Function
    End Class

    Public MapInfos = new List(Of MapInfo)

    Public Sub New(inkNames As String(), inkTowers As Integer())
        Debug.Assert(inkNames.Length = inkTowers.Length)

        For i = 0 To inkNames.Length - 1
            Dim mapInfo As new MapInfo(inkNames.ElementAt(i), inkTowers.ElementAt(i))
            mapInfo.InkName = inkNames.ElementAt(i)
            mapInfo.InkTower = inkTowers.ElementAt(i)
            MapInfos.Add(mapInfo)
        Next

        ' calc the colorbarEditorIndices
        ' K = 0, C = 1, M = 2, Y = 3
        ' spot colors must start at 4, so S1 = 3 + 1, S2 = 3 + 2, etc.
        For i = 0 To MapInfos.Count - 1
            Dim tempMapInfo = MapInfos(i)

            If (IsNothing(tempMapInfo.InkName)) Then
                Continue For
            End If

            Select Case tempMapInfo.InkName.Substring(0, 1)
                Case "K"
                    tempMapInfo.ColorBarEditorIndex = 0
                Case "C"
                    tempMapInfo.ColorBarEditorIndex = 1
                Case "M"
                    tempMapInfo.ColorBarEditorIndex = 2
                Case "Y"
                    tempMapInfo.ColorBarEditorIndex = 3

                Case "S" ' spot colors
                    Dim spotColorIndex = CInt(tempMapInfo.InkName.Substring(1, tempMapInfo.InkName.Length - 1))
                    tempMapInfo.ColorBarEditorIndex = 3 + spotColorIndex

                Case Else
                    tempMapInfo.ColorBarEditorIndex = -1
            End Select

        Next
    End Sub

    Public Function GetColorBarEditorIndexForTowerIndex(towerIndex As String) As String
        If String.IsNullOrWhiteSpace(towerIndex) Then
            Return towerIndex
        End If

        For i = 0 To MapInfos.Count - 1
            If (MapInfos(i).InkTower = CInt(towerIndex)) Then
                Return MapInfos(i).ColorBarEditorIndex
            End If
        Next

        Return -1
    End Function
End Class