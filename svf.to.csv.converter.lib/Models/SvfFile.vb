﻿Public Class SvfFile
    Public PressName As String              ' Pressname as indicated in the original SVF
    Public Keys As Integer                  ' Number of ink keys of this press as indicated in the original SVF
    Public Keysize As Double                ' Key width of the Press as indicated in the original SVF
    Public PatchWidth As Double             ' Patch width to be calculated on the go
    Public PatchesPerZone As Integer        ' Patches per zone
    Public StartKey As Integer              ' The first found ink key of the measurement
    Public UsedKeys As Integer              ' Total found ink keys in this measurement
    Public InkSlots As Integer              ' ink slots of the press as indicated in the original SVF
    Public InkNumber As Integer             ' number of inks used in this measurement
    Public OriginalColorBarName As String   ' color bar name as indicated in the original SVF
End Class