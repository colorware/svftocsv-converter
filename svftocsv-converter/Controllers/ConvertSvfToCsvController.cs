﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

using svf.to.csv.azure.mail.Enums;
using svf.to.csv.azure.mail.MailJet;
using svf.to.csv.converter.lib;

using svftocsv_converter.Models;

namespace svftocsv_converter.Controllers {
    public class ConvertSvfToCsvController : Controller {
        public ActionResult Index() {
            var model = new SvfFileUploadModel();
            return View(model);
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public ActionResult Convert(SvfFileUploadModel model) {
            if (!string.IsNullOrWhiteSpace(model.File?.FileName)) {
                string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                string filename = Path.GetFileName(model.File?.FileName);

                string absoluteFilename = Path.Combine(path, filename);
                model.File?.SaveAs(absoluteFilename);

                string csvFilename = convertToCsv(absoluteFilename, model.ColorBarName, model.ColorBarDescription,
                                                  model.PatchHeight, model.EmailRecipients, out var messages);
                TempData["LastUploadFilename"] = Path.GetFileName(csvFilename);
                TempData["LastUploadAbsPath"] = csvFilename;
                TempData["Messages"] = messages;
            }

            return RedirectToAction("Downloads");
        }

        //---------------------------------------------------------------------

        public ActionResult Downloads() {
            var dir = new DirectoryInfo(Server.MapPath("~/App_Data/uploads/"));
            if (!dir.Exists) {
                dir.Create();
            }

            var files = dir.GetFiles("*.*").ToList();
            return View(files);
        }

        //---------------------------------------------------------------------

        public FileResult Download(string fileName) {
            var fileVirtualPath = "~/App_Data/uploads/" + Path.GetFileName(fileName);
            return File(fileVirtualPath, System.Net.Mime.MediaTypeNames.Application.Octet,
                        Path.GetFileName(fileVirtualPath));
        }

        //---------------------------------------------------------------------

        public ActionResult Delete(string fileName) {
            var fileVirtualPath = Path.Combine(Server.MapPath("~/App_Data/uploads/"), fileName);
            System.IO.File.Delete(fileVirtualPath);
            return RedirectToAction("Downloads");
        }

        //---------------------------------------------------------------------

        private static string convertToCsv(string svfFilename, string colorBarName, string colorBarDescr,
                                           double patchHeight, string emailRecipients,
                                           out string messages) {
            messages = "";
            var emailService = new MailJetWrapper();

            // convert to CSV and download
            SvfFileImporter svfFileImporter = new SvfFileImporter();
            DataTable data = null;
            try {
                svfFileImporter.ImportSvf(svfFilename);
            }
            catch (Exception e) {
                if (svfFileImporter.Messages.Length > 0) {
                    messages = $"Import Errors:  {svfFileImporter.Messages}";
                }

                emailService.Send(MessageType.SvfToCsvConversion_Failure, emailRecipients, svfFilename, "",
                                  e.Message);

                return "";
            }


            data = svfFileImporter.Data;
            CsvFileExporter csvFileExporter = new CsvFileExporter();
            var csvDirectory = buildCsvDirectory(svfFilename);
            string csvFilename = "";
            try {
                // TODO - SvfFile must read it's own data / create internal data table
                //        along with other properties...
                csvFilename = csvFileExporter.Export(data, colorBarName, colorBarDescr,
                                                     svfFileImporter.SvfFile.PatchWidth, patchHeight,
                                                     svfFileImporter.SvfFile.PatchesPerZone,
                                                     svfFileImporter.SvfFile.UsedKeys, csvDirectory);
            }
            catch (Exception e) {
                if (csvFileExporter.Messages.Length > 0) {
                    if (messages.Length > 0) messages += $"{Environment.NewLine}{Environment.NewLine}";
                    messages = $"Export Errors:  {csvFileExporter.Messages}";
                }

                emailService.Send(MessageType.SvfToCsvConversion_Failure, emailRecipients, svfFilename, "",
                                  e.Message);
            }

            emailService.Send(MessageType.SvfToCsvConversion_Success, emailRecipients, svfFilename, csvFilename,
                              "");

            return csvFilename;
        }

        private static string buildCsvDirectory(string svfFilename) {
            string directory = Path.GetDirectoryName(svfFilename) ?? @"c:\temp";

            //string filename = Path.GetFileNameWithoutExtension(svfFilename) ?? "temp";
            //var csvFilename = Path.Combine(directory, filename) + ".csv";
            //return csvFilename;
            return directory;
        }
    }
}