﻿using System.ComponentModel;
using System.Web;

namespace svftocsv_converter.Models {
    public class SvfFileUploadModel {
        [DisplayName("SVF File")]
        public HttpPostedFileBase File { get; set; }

        [DisplayName("Patch Height (mm)")]
        public double PatchHeight { get; set; }

        [DisplayName("Color Bar Name")]
        public string ColorBarName { get; set; }

        public string DefaultColorBarName => "Color Bar Name";

        [DisplayName("Color Bar Description")]
        public string ColorBarDescription { get; set; }

        public string DefaultColorBarDescription => "Description";

        [DisplayName("Email Recipients")]
        public string EmailRecipients { get; set; }

        public SvfFileUploadModel() {
            PatchHeight = 8;
            ColorBarName = DefaultColorBarName;
            ColorBarDescription = DefaultColorBarDescription;
        }
    }
}